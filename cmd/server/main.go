package main

import (
	"log"

	"gitlab.com/MixedMachine/proglog/internal/server"
)

func main() {
	srv := server.NewHTTPServer(":8910")
	log.Fatal(srv.ListenAndServe())
}
